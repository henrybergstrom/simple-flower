import hypermedia.net.*;    // import UDP library


float increment = 0.02;
int PORT_RX=8888;
String HOST_IP = "127.0.0.1";//IP Address of the PC in which this App is running
UDP udp;//Create UDP object for recieving
float average;
float modifier  = 15;
float floatingAverage = 0.0;


// TURTLE STUFF:
float x, y;     // the current position of the turtle
float currentangle = 0;   // which way the turtle is pointing
float step = 1.0;      // how much the turtle moves with each 'F'
float angle = 45.0;

// LINDENMAYER STUFF (L-SYSTEMS)
String thestring = "0"; // "axiom" or start of the string
int numloops = 8; // how many iterations to pre-compute
//String[][] therules = new String[2][2];     // array for rules
HashMap<Character, String> therules = new HashMap();
int whereinstring = 0; // where in the L-system are we?

void setup() {
  therules.put('1', "11");    
  therules.put('0', "1[0]0");  
  therules.put('[', "[");
  therules.put(']', "]"); 
  size(640, 360);
  background(0);
  smooth();
  udp= new UDP(this, PORT_RX, HOST_IP);
  udp.log(false);
  udp.listen(true);
  generate();
}

void receive( byte[] data, String ip, int port ) {  // <-- extended handler
  // get the "real" message
  String message = new String( data );
  message = message.substring(1, message.indexOf(']')); // trimmed of [ and ]
  String[] parts = split(message, ", ");                // split data intro parts  
  //println(message);
  float totalof3parts = Float.parseFloat(parts[1]) + Float.parseFloat(parts[2]) + Float.parseFloat(parts[3]);
  average = totalof3parts/3;
  
  floatingAverage =  (floatingAverage * 4 + average) / 5;
}

void draw() {
  background(0);
  //stroke (100);
  //ellipse(width * 0.5 + floatingAverage * modifier, height * 0.5, 64, 64);
  println("floatingAverage -> " + floatingAverage);

  angle = floatingAverage * modifier;
  // L draw
  drawIt();
  resetMatrix();

}


void generate() {
    // COMPUTE THE L-SYSTEM
    for (int i = 0; i < numloops; i++) {
        thestring = lindenmayer(thestring);
    }
}

// interpret an L-system
String lindenmayer(String s) {
    StringBuilder outputString = new StringBuilder(); // start a blank output string

    // iterate through 'therules' looking for symbol matches:
    for (int i = 0; i < s.length(); i++) {
      char c = s.charAt(i);
      String ruleResult = therules.get(c);
      outputString.append(ruleResult);
    }
    
    println(outputString.toString());
    return outputString.toString(); // send out the modified string
}

// this is a custom function that draws turtle commands
void drawIt() {
    background(151);
    translate(width / 2, height);
    stroke(255, 100);
    for (int i = 0; i < thestring.length(); i++) {
      char c = thestring.charAt(i);
      if (c == '0') {
          line(0, 0, 0, -step);
      } else if (c == '1') {
          line(0, 0, 0, -step);
          translate(0, -step);
      } else if (c == '[') {
          push();
          step *= 1.;
          rotate(-angle);
      } else if (c == ']') {
          pop();
          rotate(angle);
      }
    }
}
