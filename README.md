# SIMPLE FLOWER
This is a prototype project to showcase how to use the OpenBCI_Python API to access the data from a OpenBCI Ganglion Board connected to a human brain. T
he project was made possible by the help of [MindLAB](mindlab.space) and [PurpleScout](www.purplescout.se)

# Prerequisite
- Python2.7 with pip
- A OpenBCI Ganglion board
- Processing Java Framework

# Set up
1. Connect the Ganglian Board and make sure at least one channel is working. Verify using the OpenBCI_GUI.
2. Set up OpenBCI_Python API
3. Install dependencies `python2.7 -m pip -r install requirements.txt.henry`
4. Run `cd OpenBCI_API && python2.7 user.py --board ganglion -p CB:C7:7C:64:4C:EE --add udp_server`. The data is now streaming through an UDP server from localhost, port 8888.
5. Run `python2.7 scripts/UDP_test.py`. The output should look like this.
![](assets/udp_output.gif)
6. Open up Processing and launch `Processing simple_flower/L_system_openbci_api`. You should have something like this.
![](assets/flower_output.gif)


